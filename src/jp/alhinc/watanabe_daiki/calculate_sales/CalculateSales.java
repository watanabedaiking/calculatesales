package jp.alhinc.watanabe_daiki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchNames = new HashMap<>();//支店定義ファイルのマップを宣言
		Map<String, Long> branchSales = new HashMap<>();//売り上げファイルのマップを宣言

		System.out.println("ここにあるファイルを開きます=>" + args[0]);//指定したファイルのパスを開く。

			//コマンドライン引数が1つ設定されていない場合
			if(args.length != 1) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			if(! fileReader(args[0],"branch.lst","支店定義","^[0-9]{3}$",branchNames,branchSales)) {
				return;
			}

		File[] files = new File(args[0]).listFiles();
		//listFilesを使用して、filesという配列に指定したパスに存在する
		//すべてのファイル（またはディレクトリ）の情報が格納します。
		List<File> rcdFiles = new ArrayList<>();
		//ファイルの情報を格納するList(ArrayList)を宣言します。

		for (int i = 0; i < files.length; i++) {
			//filesの数だけ指定したパスに存在するすべてのファイルの数だけ繰り返す。
			//指定した売り上げ集計課題のファイルの数は７個だから７回繰り返す。
			if (files[i].getName().matches("^[0-9]{8}.+rcd$") &&  files[i].isFile()) {
				//数字が8桁でrcdという名前のフォルダを探す。かつrcdFilesの中にフォルダが混じっていた場合

				rcdFiles.add(files[i]);
				//売り上げファイルの条件に当てはまったものだけ、List(ArrayList)に追加する。
			}
		}

		//rcdFiles（売り上げファイル）が連番になっていなかったときのエラー処理
		for(int i = 0; i < rcdFiles.size() - 1;i++) {
				int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
				int later = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

				if(later - former != 1) {
					System.out.println("売り上げファイルが連番になっていません");
					return;
				}
		}

		BufferedReader brBranchSales = null;//ここから、rcdファイルの読み込みと集計の処理
		for (int i = 0; i < rcdFiles.size(); i++) {//rcdファイルの数だけ繰り返す。
			try {
				FileReader frBranchSales;
				frBranchSales = new FileReader(rcdFiles.get(i));

				brBranchSales = new BufferedReader(frBranchSales);

				String line;//String型のlineという変数の宣言
				List<String> items = new ArrayList<>();//itemsというリストの箱を用意する

				while ((line = brBranchSales.readLine()) != null) {//中身が無くなるまで繰り返す。
					System.out.println(line);//line変数を表示する
					items.add(line);//読み込んだline変数をitemsというリストに入れて使えるようにする。
				}

				//売り上げファイルの中身が2行以外の場合
				if(items.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				String branchCode = items.get(0);//支店コード
				String sales = items.get(1);//売上金額

				//該当ファイル名の支店コードがなかった場合
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//売上ファイルを読み込んだ際に売上金額をLong型に変換している為、売上金額が数字なのか確認
				if(! sales.matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long filesale = Long.parseLong(sales);//売上金額の型の変換
				Long saleAmount = branchSales.get(branchCode) + filesale;//Mapから値を加算する

				//合計金額が10桁を超えた場合
				if(saleAmount >= 1000000000L) {
					System.out.println("売上金額が10桁を超えました");
					return;
				}
				branchSales.put(branchCode,saleAmount);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (brBranchSales != null) {
					try {
						brBranchSales.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		//共通ファイル出力メソッドの呼び出し
		if(fileWriter(args[0],"branch.out",branchNames,branchSales)) {
			return;
		}
	}

//共通ファイル入力メソッド（ファイルを読み込むメソッド）
	public static boolean fileReader(String filePath, String fileName, String definition, String regular, Map<String, String> namesMap, Map<String, Long> salesMap) {
		BufferedReader br = null;//一時的な保存場所
		try {
			File file = new File(filePath, fileName);//コマンドライン引数で渡したパスを指定し、指定されたファイルを開き、中身を表示する
			FileReader fr = new FileReader(file);//ファイルリーダーオブジェクトの作成
			br = new BufferedReader(fr);//ファイルリーダーオブジェクトを引数として、バッファーリーダーオブジェクトの作成

			//"支店定義"ファイルが存在しない場合
			if(!file.exists()) {
				System.out.println(definition + "ファイルが存在しません");
				return false;
			}

			String line;//String型のlineという変数を宣言
			while ((line = br.readLine()) != null) {//brの中身の行が空になるまで繰り返す。
				System.out.println(line);//読み込んだ行を表示する
				String[] items = line.split(",");//文字列をカンマで区切る。

			//支店定義ファイルのフォーマットが不正の場合
			if(items.length != 2 || (! items[0].matches(regular))) {
				System.out.println(definition + "ファイルのフォーマットが不正です");
				return false;
				}

				namesMap.put(items[0], items[1]);//Mapの値に追加する。第一引数：支店コード、第二引数：支店名
				salesMap.put(items[0], 0L);//Mapの値に追加する。第一引数：支店コード、第二引数：金額
				//Mapに追加する2つの情報をputの引数として指定します。

			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


//共通ファイル出力メソッド（ファイルに書き込むメソッド）
	public static boolean fileWriter(String filePath, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {

		BufferedWriter bw = null;
		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key : salesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}



